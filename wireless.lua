

local io = io
local tonumber = tonumber
local string = string


module("wireless")
local device

function initialize()
   device = "wlan0"
end

function noise()
   local fh
   local noise = 0
   fh = io.open("/sys/class/net/"..device.."/wireless/noise")
   if fh ~= nil then
      noise = fh:read()
      fh:close()
   end
   return tonumber(noise)
end


function quality()
   local fh
   local quality = 0
   fh = io.open("/sys/class/net/"..device.."/wireless/link")
   if fh ~= nil then
      quality = fh:read()
      fh:close()
   end
   return tonumber(quality)
end


function address()
   local fh
   local address = ""
   fh = io.open("/sys/class/net/"..device.."/address")
   if fh ~= nil then
      address = fh:read()
      fh:close()
   end
   return address
end

function essid()
   local iwconfig = io.popen("/sbin/iwconfig "..device)
   local line = iwconfig:read()
   local essid = string.match(line, "ESSID:\"(.*)\"")
   
   return essid
end
