---------------------------
-- Default awesome theme --
---------------------------

theme = {}

theme.font          = "Terminus 8"

theme.bg_normal     = "#222222"
--theme.bg_focus      = "#535d6c"
theme.bg_focus      = "#222222"
--theme.bg_urgent     = "#ff0000"
theme.bg_urgent     = "#222222"
theme.bg_minimize   = "#444444"

theme.fg_normal     = "#aaaaaa"
theme.fg_focus      = "#ffffff"
theme.fg_urgent     = "#ddcc00"
theme.fg_minimize   = "#ffffff"

theme.border_width  = "1"
theme.border_normal = "#000000"
theme.border_focus  = "#535d6c"
theme.border_marked = "#91231c"

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- [taglist|tasklist]_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- Example:
theme.taglist_bg_focus = "#222222"--"#990099"
theme.taglist_fg_focus = "#ffffff"
theme.taglist_fg_urgent = "#eeee00"--"#aaaafe"

theme.tooltip_border_width = "3"
theme.tooltip_border_color = "#ffcb60" --ddcc00"
theme.tooltip_bg_color = "#ffcb60" --ddcc00"
theme.tooltip_fg_color = "#222200"

-- Display the taglist squares
theme.taglist_squares_sel   = "/usr/share/awesome/themes/default/taglist/squarefw.png"
theme.taglist_squares_unsel = "/usr/share/awesome/themes/default/taglist/squarew.png"

theme.tasklist_floating_icon = "/usr/share/awesome/themes/default/tasklist/floatingw.png"

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = "~/.config/awesome/themes/tom/submenu.png"
theme.menu_height = "12"
theme.menu_width  = "120"
theme.menu_border_width = "0"


-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.titlebar_close_button_normal = "~/.config/awesome/themes/tom/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = "~/.config/awesome/themes/tom/titlebar/close_focus.png"

theme.titlebar_ontop_button_normal_inactive = "~/.config/awesome/themes/tom/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = "~/.config/awesome/themes/tom/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = "~/.config/awesome/themes/tom/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = "~/.config/awesome/themes/tom/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = "~/.config/awesome/themes/tom/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = "~/.config/awesome/themes/tom/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = "~/.config/awesome/themes/tom/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = "~/.config/awesome/themes/tom/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = "~/.config/awesome/themes/tom/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = "~/.config/awesome/themes/tom/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = "~/.config/awesome/themes/tom/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = "~/.config/awesome/themes/tom/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = "~/.config/awesome/themes/tom/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = "~/.config/awesome/themes/tom/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = "~/.config/awesome/themes/tom/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = "~/.config/awesome/themes/tom/titlebar/maximized_focus_active.png"

-- You can use your own command to set your wallpaper
--theme.wallpaper_cmd = { "awsetbg ~/.config/awesome/themes/tom/background.png" }
--theme.wallpaper_cmd = { "awsetbg -r ~/Imágenes/wallpapers/" }
--theme.wallpaper_cmd = { "awsetbg -c  /usr/share/awesome/themes/default/background_white.png" }
-- theme.wallpaper_cmd = { "awsetbg -c  ~/.config/awesome/themes/tom/background_white1024.png" }
theme.wallpaper_cmd = { "nitrogen --restore" }

-- You can use your own layout icons like this:
theme.layout_fairh = "~/.config/awesome/themes/tom/layouts/fairh.png"
theme.layout_fairv = "~/.config/awesome/themes/tom/layouts/fairv.png"
theme.layout_floating  = "~/.config/awesome/themes/tom/layouts/floating.png"
theme.layout_magnifier = "~/.config/awesome/themes/tom/layouts/magnifier.png"
theme.layout_max = "~/.config/awesome/themes/tom/layouts/max.png"
theme.layout_fullscreen = "~/.config/awesome/themes/tom/layouts/fullscreen.png"
theme.layout_tilebottom = "~/.config/awesome/themes/tom/layouts/tilebottom.png"
theme.layout_tileleft   = "~/.config/awesome/themes/tom/layouts/tileleft.png"
theme.layout_tile = "~/.config/awesome/themes/tom/layouts/tile.png"
theme.layout_tiletop = "~/.config/awesome/themes/tom/layouts/tiletop.png"
theme.layout_spiral  = "~/.config/awesome/themes/tom/layouts/spiral.png"
theme.layout_dwindle = "~/.config/awesome/themes/tom/layouts/dwindle.png"

theme.awesome_icon = "/usr/share/awesome/icons/awesome16.png"

--iconos para netwidget
theme.widget_net = "/usr/shareicons/gnome-brave/16x16/actions/go-down.png"
theme.widget_netup = "/usr/share/icons/gnome-brave/16x16/actions/go-up.png"


return theme
-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:encoding=utf-8:textwidth=80
