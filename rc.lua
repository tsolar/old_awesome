-- Standard awesome library
require("awful")
require("awful.autofocus")
require("awful.rules")
-- Theme handling library
require("beautiful")
-- Notification library
require("naughty")

-- Load Debian menu entries
require("debian.menu")

--config
config = awful.util.getdir("config")

-- {{{ Variable definitions
-- Themes define colours, icons, and wallpapers
beautiful.init(config .. "/themes/tom/theme.lua")

-- This is used later as the default terminal and editor to run.
terminal = "x-terminal-emulator"
editor = os.getenv("EDITOR") or "editor"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
layouts =
{
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.spiral,
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    awful.layout.suit.magnifier
}
-- }}}


--------------------------------------------yo

---------------------------------------yo

---freedesktop from arch...
-- applications menu
require("freedesktop.utils")
freedesktop.utils.terminal = terminal  -- default: "xterm"
freedesktop.utils.icon_theme = "gnome" -- look inside /usr/share/icons/, default: nil (don't use icon theme)
require("freedesktop.menu")
--  require("debian.menu")

-- applications menu
-- require("awesome-freedesktop.freedesktop.utils")
-- freedesktop.utils.terminal = terminal  -- default: "xterm"
-- freedesktop.utils.icon_theme = "gnome" -- look inside /usr/share/icons/, default: nil (don't use icon theme)
-- require("awesome-freedesktop.freedesktop.menu")
-- require("debian.menu")


-- here is a function created by Matthew Hague
-- to keep dialog boxes on current mouse screen
-- (where the mouse pointer is located at)
function totag(i)
   return function(c)
             local screentags = screen[mouse.screen]:tags()
             c:tags({screentags[i]})
             c.screen = mouse.screen
             awful.tag.viewonly(screentags[i])
             naughty.notify({
                text = "ESTOY ENTRANDO A ESTA COSA",
                timeout = 0,
                hover_timeout = 0.5,
                screen = capi.mouse.screen,
        })
          end
end


--mio
mysystemmenu = {
   { "suspend" , "gksu pm-suspend" },
   { "hibernate" , "gksu pm-hibernate" },
   { "reboot" , "gksu 'shutdown -r now'" },
   { "shutdown" , "gksu 'shutdown -h now'" }
}
--fin mio
freqapps = {
   { "emacs", "emacs" },
--   { "claws-mail" , "claws-mail" },
   { "pidgin" , "pidgin" },
   { "icecat" , "icecat" },
   { "ncmpcpp" , terminal .. " -e ncmpcpp" },
   { "netbeans" , "bash /usr/local/netbeans-7.0/bin/netbeans &" },
--   { "Invaders", invaders.run() }
}


menu_items = freedesktop.menu.new()
myawesomemenu = {
   { "manual", terminal .. " -e man awesome", freedesktop.utils.lookup_icon({ icon = 'help' }) },
   { "edit config", editor_cmd .. " " .. awful.util.getdir("config") .. "/rc.lua", freedesktop.utils.lookup_icon({ icon = 'package_settings' }) },
   { "restart", awesome.restart, freedesktop.utils.lookup_icon({ icon = 'gtk-refresh' }) },
   { "quit", awesome.quit, freedesktop.utils.lookup_icon({ icon = 'gtk-quit' }) }
}
table.insert(menu_items, { "awesome", myawesomemenu, beautiful.awesome_icon })
table.insert(menu_items, { "System" , mysystemmenu,  })
table.insert(menu_items, { "Debian", debian.menu.Debian_menu.Debian })
table.insert(menu_items, { "Frecuentes", freqapps })
table.insert(menu_items, { "open terminal", terminal, freedesktop.utils.lookup_icon({icon = 'terminal'}) })


mymainmenu = awful.menu.new({ items = menu_items, width = 150, height = 16 })


naughty.config.presets.normal.border_width = "0"
naughty.config.presets.normal.fg = beautiful.tooltip_fg_color
naughty.config.presets.normal.bg = beautiful.tooltip_bg_color
naughty.config.presets.normal.icon_size = 48
naughty.config.presets.normal.opacity = 0.9


-- dynamic tagging
require("eminent")
--require("shifty")

-- para algunos widgets
require("wicked")
require("vicious")

require("obvious")
-- require("obvious.loadavg")
require("obvious.volume_alsa")
--volumewidget = obvious.volume_alsa():set_layout(awful.widget.layout.horizontal.rightleft).widget


require("calendar2")


spacer = widget({ type = "textbox"  })
spacer.text = " | "

space = widget({ type = "textbox"  })
space.text = " "


--thermal widget
thermalwidget = widget({ type = 'textbox' })
vicious.register(thermalwidget, vicious.widgets.thermal, '$1°C', 5, 'thermal_zone0')
--fin thermal widget


--cpuwidget
cpuwidget = widget({ type = 'textbox' })
vicious.register(cpuwidget, vicious.widgets.cpu, "$1%", 3)
cpuwidget:buttons(awful.util.table.join(
  awful.button({            }, 1, function () awful.util.spawn( terminal .. " -e htop") end)
))

cpuicon = widget({ type = 'imagebox' })
cpuicon.image = image(config .. "/icons/cpu2.png")
--fin cpuwidget

--weather widget
weatherwidget = widget({ type = 'textbox' })
vicious.register(weatherwidget, vicious.widgets.weather, '${tempc}°C', 360, 'SCEL')
weatherwidget:add_signal('mouse::enter', function ()
                                            local statf = io.popen('weather -i SCEL -m')
                                            -- weather-util  -i SCEL -m --headers weather |grep Weather |cut -d ":" -f2 |cut -d " " -f 2
                                            -- para el icono... ^^^
                                            local stat = statf:read("*all")
                                            statf :close()
                                            weatherinfo = {
                                               naughty.notify({ 
                                                                 title      = "Weather"
                                                                 , text       = stat
                                                                 , timeout    = 0
                                                                 , position   = "top_right"
                                                                 --, fg = beautiful.tooltip_fg_color
                                                                 --, bg = beautiful.tooltip_bg_color
                                                                 --, border_width = "0"
                                                             })
                                            }
                                         end)
weatherwidget:add_signal('mouse::leave', function () naughty.destroy(weatherinfo[1]) end)

--fin weather widget

---weather widget icon

weathericon = widget({
    type = 'textbox',
    name = 'weathericon'
})

function weather_icon()
   local f = io.popen('conkyForecast --location=CIXX0020  --datatype=WF')
   local value = f:read()
   f:close()
   
   return {value}
end


--wicked.register(weathericon, weather_icon, "<span color='white' font='ConkyWeather'>1</span>", 4000)

---fin weather widget icon

---battery widget...
batwidget = widget({ type = "textbox" })
--[[
vicious.register(batwidget, vicious.widgets.bat, 
                 function (widget, args)-- "$2% $3"
                    local percent = args[2] .. "%"
                    if args[3] == 100 then
                       return "ciennnn"
                    else return percent .. " " .. args[3]
                    end
                 end
                 , 61, "BAT0")

--]]
batwidget:add_signal('mouse::enter', function()
                                        local fd = nil
                                        fd = io.popen("acpi -btai")
                                        local d = fd:read("*all"):gsub("\n+$", "")
                                        fd:close()
                                        batinfo = {
                                           naughty.notify({
                                                             text         = d
                                                             , timeout    = 0
                                                             , position   = "bottom_right"
                                                             --, fg = beautiful.tooltip_fg_color 
                                                             --, bg = beautiful.tooltip_bg_color
                                                             --, border_width = "0"
                                                       })
                                        }
                                     end)

batwidget:add_signal('mouse::leave', function () naughty.destroy(batinfo[1]) end)
--- fin battery widget


--baticon widget
--iconos para baticon widget
beautiful.widget_baticon_bat1 = config .. "/icons/battery/bat1.png"
beautiful.widget_baticon_bat2 = config .. "/icons/battery/bat2.png"
beautiful.widget_baticon_bat3 = config .. "/icons/battery/bat3.png"
beautiful.widget_baticon_bat4 = config .. "/icons/battery/bat4.png"
beautiful.widget_baticon_bat5 = config .. "/icons/battery/bat5.png"
beautiful.widget_baticon_bat6 = config .. "/icons/battery/bat6.png"
beautiful.widget_baticon_bat7 = config .. "/icons/battery/bat7.png"
beautiful.widget_baticon_bat8 = config .. "/icons/battery/bat8.png"
beautiful.widget_baticon_bat9 = config .. "/icons/battery/bat9.png"
beautiful.widget_baticon_bat10 = config .. "/icons/battery/bat10.png"
beautiful.widget_baticon_bat11 = config .. "/icons/battery/bat11.png"

beautiful.widget_baticon_char1 = config .. "/icons/battery/char1.png"
beautiful.widget_baticon_char2 = config .. "/icons/battery/char2.png"
beautiful.widget_baticon_char3 = config .. "/icons/battery/char3.png"
beautiful.widget_baticon_char4 = config .. "/icons/battery/char4.png"
beautiful.widget_baticon_char5 = config .. "/icons/battery/char5.png"
beautiful.widget_baticon_char6 = config .. "/icons/battery/char6.png"
beautiful.widget_baticon_char7 = config .. "/icons/battery/char7.png"
beautiful.widget_baticon_char8 = config .. "/icons/battery/char8.png"
beautiful.widget_baticon_char9 = config .. "/icons/battery/char9.png"
beautiful.widget_baticon_char10 = config .. "/icons/battery/char10.png"

beautiful.widget_baticon_ac = config .. "/icons/battery/ac.png"


--iconos para baticon widget \o/
baticon = widget({ type = "imagebox" })
baticon_t = awful.tooltip({ objects = { baticon }, })
--baticon.image = image(beautiful.baticon_bat1)
vicious.register(baticon, vicious.widgets.bat, function (widget, args)
                                                  baticon_t:set_text(args[1] .." ".. args[2].."% " .. args[3] )
                                                  
                                                  local battery_status = ""
                                                  battery_status = args[1]
                                                  if not battery_status then 
                                                     baticon.image = image(beautiful.widget_baticon_ac)
                                                  elseif args[1] == "-" then --battery_status == "discharging" then

                                                     if args[2] > 95 and args[2] <= 100 then
                                                        baticon.image = image(beautiful.widget_baticon_bat1)

                                                     elseif args[2] > 90 and args[2] < 95 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_bat2)
                                                     elseif args[2] >= 80 and args[2] < 90 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_bat3)
                                                     elseif args[2] >= 70 and args[2] < 80 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_bat3)
                                                     elseif args[2] >= 60 and args[2] < 70 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_bat4)
                                                     elseif args[2] >= 50 and args[2] < 60 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_bat5)
                                                     elseif args[2] >= 40 and args[2] < 50 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_bat6)
                                                     elseif args[2] >= 30 and args[2] < 40 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_bat7)
                                                     elseif args[2] >= 20 and args[2] < 30 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_bat8)
                                                     elseif args[2] >= 10 and args[2] < 20 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_bat9)
                                                     elseif args[2] > 5 and args[2] < 10 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_bat10)

                                                     elseif args[2] <= 5 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_bat11)
                                                     end

                                                  elseif args[1] == "+" then--battery_status == "charging" then
                                                     if args[2] > 95 and args[2] <= 100 then
                                                        baticon.image = image(beautiful.widget_baticon_char1)
                                                     elseif args[2] > 90 and args[2] < 95 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_char2)
                                                     elseif args[2] >= 80 and args[2] < 90 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_char3)
                                                     elseif args[2] >= 70 and args[2] < 80 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_char3)
                                                     elseif args[2] >= 60 and args[2] < 70 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_char4)
                                                     elseif args[2] >= 50 and args[2] < 60 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_char5)
                                                     elseif args[2] >= 40 and args[2] < 50 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_char6)
                                                     elseif args[2] >= 30 and args[2] < 40 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_char7)
                                                     elseif args[2] >= 20 and args[2] < 30 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_char8)
                                                     elseif args[2] >= 10 and args[2] < 20 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_char9)
                                                     elseif args[2] > 5 and args[2] < 10 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_char10)

                                                     elseif args[2] <= 5 then
                                                        
                                                        baticon.image = image(beautiful.widget_baticon_char10)
                                                     end
                                                     --batwidget.text = args[2] .. "% " .. args[3]
                                                  elseif args[1] == "↯" then
                                                     baticon.image = image(beautiful.widget_baticon_ac)
                                                     --if args[2] == 100 then batwidget.text = "cien " end
                                                  end



                                                  if args[2] == 100 and args[1] ~= "-" and args[1] ~= "+" then
                                                     batwidget.text = "Full"--args[2] .. "%"
                                                  else
                                                     batwidget.text = args[2] .. "% " .. args[3]
                                                  end


                                                  if args[1] == "⌁" then --battery_present == '0' then
                                                     baticon.image = image(beautiful.widget_baticon_ac)
                                                     batwidget.text = "N/P"
                                                  end
                                               end,
                 1, "BAT0")





--fin baticon widget


--netwidget
-- Initialize widget
netwidget = widget({ type = "textbox" })
-- Register widget
vicious.register(netwidget, vicious.widgets.net, function(widget, args)
                                                    local statf = io.popen('LC_ALL=C nmcli dev | grep \'con[ne]\' | cut -d\' \' -f1')
                                                    local stat = statf:read("*l")
                                                    statf :close()
                                                    return '<span color="#CC9393">'..
						       args["{".. stat .. " down_kb}"] ..
						       '</span> <span color="#7F9F7F">' ..
						       args["{" .. stat .. " up_kb}"] .. '</span>'
                                                 end, 3)
netwidget:buttons(awful.util.table.join(
  awful.button({ }, 1, function () awful.util.spawn( terminal .. " -e wicd-curses") end)
))
--iconos para netwidget
--beautiful.widget_netdown = config .. "/go-down12.png"
--beautiful.widget_netup   = config .. "/go-up12.png"
beautiful.widget_netdown = config .. "/icons/down.png"
beautiful.widget_netup   = config .. "/icons/up.png"

--iconos para netwidget \o/
dnicon = widget({ type = "imagebox" })
upicon = widget({ type = "imagebox" })
dnicon.image = image(beautiful.widget_netdown)
upicon.image = image(beautiful.widget_netup)



-- run or raise!
--- Spawns cmd if no client can be found matching properties
-- If such a client can be found, pop to first tag where it is visible, and give it focus
-- @param cmd the command to execute
-- @param properties a table of properties to match against clients.  Possible entries: any properties of the client object
function run_or_raise(cmd, properties)
   local clients = client.get()
   local focused = awful.client.next(0)
   local findex = 0
   local matched_clients = {}
   local n = 0
   for i, c in pairs(clients) do
      --make an array of matched clients
      if match(properties, c) then
         n = n + 1
         matched_clients[n] = c
         if c == focused then
            findex = n
         end
      end
   end
   if n > 0 then
      local c = matched_clients[1]
      -- if the focused window matched switch focus to next in list
      if 0 < findex and findex < n then
         c = matched_clients[findex+1]
      end
      local ctags = c:tags()
      if table.getn(ctags) == 0 then
         -- ctags is empty, show client on current tag
         local curtag = awful.tag.selected()
         awful.client.movetotag(curtag, c)
      else
         -- Otherwise, pop to first tag client is visible on
         awful.tag.viewonly(ctags[1])
      end
      -- And then focus the client
      client.focus = c
      c:raise()
      return
   end
   awful.util.spawn(cmd)
end

-- Returns true if all pairs in table1 are present in table2
function match (table1, table2)
   for k, v in pairs(table1) do
      if table2[k] ~= v and not table2[k]:find(v) then
         return false
      end
   end
   return true
end
-- end run or raise


--- run_once...
-- It should be avaible for every system: debian (liblua5.1-filesystem0), freebsd (ports/devel/luafilesystem/), gentoo (dev-lua/luafilesystem), ubuntu (liblua5.1-filesystem0), archlinux (luafilesystem)

-- @bsdguys: don't forget to mount procfs (: 
require("lfs") 
-- {{{ Run programm once
local function processwalker()
   local function yieldprocess()
      for dir in lfs.dir("/proc") do
        -- All directories in /proc containing a number, represent a process
        if tonumber(dir) ~= nil then
          local f, err = io.open("/proc/"..dir.."/cmdline")
          if f then
            local cmdline = f:read("*all")
            f:close()
            if cmdline ~= "" then
              coroutine.yield(cmdline)
            end
          end
        end
      end
    end
    return coroutine.wrap(yieldprocess)
end

local function run_once(process, cmd)
   assert(type(process) == "string")
   local regex_killer = {
      ["+"]  = "%+", ["-"] = "%-",
      ["*"]  = "%*", ["?"]  = "%?" }

   for p in processwalker() do
      if p:find(process:gsub("[-+?*]", regex_killer)) then
	 return
      end
   end
   return awful.util.spawn(cmd or process)
end
-- }}}
--- end run_once


run_once("nm-applet")
run_once("pidgin")
--run_once("icedove")
run_once("unagi")
run_once("udisks-glue")
run_once("conky -c .conky/conkyrc_grey ")
run_once("xscreensaver -nosplash")

-- GNOME related...
run_once("/usr/bin/gnome-settings-daemon")
run_once("/usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1")
run_once("gnome-power-manager")
run_once("system-config-printer-applet")
run_once("start-pulseaudio-x11")
run_once("/usr/bin/gnome-keyring-daemon --start --components=gpg")
run_once("/usr/bin/gnome-keyring-daemon --start --components=secrets")
run_once("/usr/bin/gnome-keyring-daemon --start --components=ssh")
run_once("/usr/bin/gnome-keyring-daemon --start --components=pkcs11")
run_once("gnome-sound-applet")
run_once("/usr/lib/notification-daemon/notification-daemon")
run_once("dropbox start")
--os.execute("bash ~/dropbox-start.sh &")
os.execute("setxkbmap us -variant altgr-intl &")
os.execute("wmname LG3D &")
--run_once("iceweasel")
run_once("emacs -q -l ~/.emacs.d/mblog.el &")

--os.execute("xrandr --output VGA-1 --mode 1280x1024 --pos 1280x0 --rotate normal --output DVI-I-1 --mode 1280x1024 --pos 0x0 --rotate normal &")
--os.execute("xcompmgr &")
-- os.execute("xscreensaver -nosplash &")
--os.execute("emacs &")
--os.execute("icecat &")
--os.execute("icedove &")
--os.execute("pidgin &")
--os.execute("/usr/local/netbeans-6.9.1/bin/netbeans &")
-- os.execute("udisks-glue &")
--localization!
--os.setlocale("es_CL.UTF-8")

--------------------------------------------fin yo

--[[
-- {{{ Tags
-- Define a tag table which hold all screen tags.
tags = {}
for s = 1, screen.count() do
    -- Each screen has its own tag table.
    tags[s] = awful.tag({ 1, 2, 3, 4, 5, 6, 7, 8, 9 }, s, layouts[1])
end
-- }}}
--]]

------------------------mis tags
tags = {
   names = {
      "term",
      "web",
      "im",
      "dev",
      "mm",
      "emacs",
      "doc",
      "misc",
      "fs"
   },
   layout = {
      awful.layout.suit.tile.bottom,--layouts[2],
      awful.layout.suit.max,--layouts[1],
      awful.layout.suit.floating,--layouts[1],
      awful.layout.suit.max,--layouts[4],
      awful.layout.suit.floating,--layouts[1],
      awful.layout.suit.max,--layouts[6],
      awful.layout.suit.max,--layouts[6],
      awful.layout.suit.floating,--layouts[5],
      awful.layout.suit.max,--layouts[6]
   }
}

for s = 1, screen.count() do
   tags[s] = awful.tag(tags.names, s, tags.layout)
end

------------------------fin mis tags

-- {{{ Menu
-- Create a laucher widget and a main menu
myawesomemenu = {
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awful.util.getdir("config") .. "/rc.lua" },
   { "restart", awesome.restart },
   { "quit", awesome.quit }
}

-- mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
--                                     { "Debian", debian.menu.Debian_menu.Debian },
--                                     { "open terminal", terminal }
--                                   }
--                         })

mylauncher = awful.widget.launcher({ image = image(beautiful.awesome_icon),
                                     menu = mymainmenu })
-- }}}

-- {{{ Wibox
-- Create a textclock widget
mytextclock = awful.widget.textclock({ align = "right" })
calendar2.addCalendarToWidget(mytextclock, "<span color='red'>%s</span>")


-- Create a systray
mysystray = widget({ type = "systray" })

-- Create a wibox for each screen and add it
mywibox = {}
myotherwibox = {}
mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
                    awful.button({ }, 1, awful.tag.viewonly),
                    awful.button({ modkey }, 1, awful.client.movetotag),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, awful.client.toggletag),
                    awful.button({ }, 4, awful.tag.viewnext),
                    awful.button({ }, 5, awful.tag.viewprev)
                    )
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  if not c:isvisible() then
                                                      awful.tag.viewonly(c:tags()[1])
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, function ()
                                              if instance then
                                                  instance:hide()
                                                  instance = nil
                                              else
                                                  instance = awful.menu.clients({ width=250 })
                                              end
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                              if client.focus then client.focus:raise() end
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                              if client.focus then client.focus:raise() end
                                          end))

for s = 1, screen.count() do
    -- Create a promptbox for each screen
    mypromptbox[s] = awful.widget.prompt({ layout = awful.widget.layout.horizontal.leftright })
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    mylayoutbox[s] = awful.widget.layoutbox(s)
    mylayoutbox[s]:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
                           awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)))
    -- Create a taglist widget
    mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.label.all, mytaglist.buttons)

    -- Create a tasklist widget
    mytasklist[s] = awful.widget.tasklist(function(c)
                                              return awful.widget.tasklist.label.currenttags(c, s)
                                          end, mytasklist.buttons)

    -- Create the wibox
    mywibox[s] = awful.wibox({ position = "top", screen = s,  height = 12 })
    -- Add widgets to the wibox - order matters
    mywibox[s].widgets = {
        {
           mylayoutbox[s],
           -- mylauncher,
           mytaglist[s],
           spacer,
           mypromptbox[s],
           layout = awful.widget.layout.horizontal.leftright
        },
        mytextclock,
        weatherwidget,
        space,
        -- space, weathericon,
        s == 1 and mysystray or nil,
        mytasklist[s],
        layout = awful.widget.layout.horizontal.rightleft
    }

    --other wibox :)
    myotherwibox[s] = awful.wibox({ position = "bottom", screen = s, height = 12 })
    -- Add widgets to the wibox - order matters
    myotherwibox[s].widgets = {
       {
          --mpdicon,
          --space,
          --mpdwidget,
	  space, obvious.volume_alsa(),
          layout = awful.widget.layout.horizontal.leftright
       },
       --memw,-- ww1, ww2,
       -- obvious.cpu(),
       --       space, cpugraphwidget,
       space, cpuwidget, cpuicon,
       space, obvious.temp_info(), 
       space, thermalwidget,
       space,
       --upcar, netwidget, dncar,
       upicon, netwidget, dnicon,
       space, obvious.battery(),
       space, baticon,
       --space, wifiwidget,
       --spacer, batwidget,
       --space,
       --batwidget,
       --space,
       --baticon,
       --     space, batterygraphwidget,
       --space, mygmail, mailicon,
       layout = awful.widget.layout.horizontal.rightleft
    }
end
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev       ),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext       ),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "w", function () mymainmenu:show({keygrabber=true}) end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.util.spawn(terminal) end),
    awful.key({ modkey, "Control" }, "r", awesome.restart),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)    end),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1)      end),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1)      end),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1)         end),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1)         end),
    awful.key({ modkey,           }, "space", function () awful.layout.inc(layouts,  1) end),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(layouts, -1) end),

    awful.key({ modkey, "Control" }, "n", awful.client.restore),

    -- Prompt
    -- awful.key({ modkey },            "r",     function () mypromptbox[mouse.screen]:run() end),
    awful.key({ modkey },            "r",     function () mypromptbox[1]:run() end),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run({ prompt = "Run Lua code: " },
                  mypromptbox[mouse.screen].widget,
                  awful.util.eval, nil,
                  awful.util.getdir("cache") .. "/history_eval")
              end)
)

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, "o",      awful.client.movetoscreen                        ),
    awful.key({ modkey, "Shift"   }, "r",      function (c) c:redraw()                       end),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end),

    --my bindings
    awful.key({                   }, "Print", function() awful.util.spawn("gnome-screenshot") end),
    awful.key({ "Mod1",           }, "Print", function() awful.util.spawn("gnome-screenshot -w") end),
    awful.key({ modkey,           }, "F8", function() awful.util.spawn("arandr") end),
    awful.key({ modkey,           }, "F9", function() awful.util.spawn("xscreensaver-command -lock") end),
    awful.key({ modkey,           }, "p", function() awful.util.spawn("rhythmbox-client --play-pause") end),
    awful.key({ modkey, "Shift"   }, "p", function() awful.util.spawn("rhythmbox-client --notify") end),
    awful.key({ modkey,           }, "b", function() awful.util.spawn("nautilus --no-desktop ") end),
    -- awful.key({ modkey, "Shift"   }, "b", function() awful.util.spawn(terminal .. " -e mc") end),
    awful.key({ modkey,           }, "g", function () run_or_raise("chromium --app='http://mail.google.com/mail/'", { name = "Gmail" }) end),
    awful.key({ modkey, "Shift"   }, "b", function () run_or_raise("xterm -name mcTerm -e mc -d", { instance = "mcTerm" }) end),

    -- unminimize windows
    awful.key({ modkey, "Shift" }, "n",
              function ()
                 local allclients = client.get(mouse.screen)
                 
                 for _,c in ipairs(allclients) do
                    if c.minimized and c:tags()[mouse.screen] ==
                    awful.tag.selected(mouse.screen) then
                    c.minimized = false
                    client.focus = c
                    c:raise()
                    return
                 end
              end
           end)    
    
    --end my bindings
    
)

-- Compute the maximum number of digit we need, limited to 9
keynumber = 0
for s = 1, screen.count() do
   keynumber = math.min(9, math.max(#tags[s], keynumber));
end

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, keynumber do
    globalkeys = awful.util.table.join(globalkeys,
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = mouse.screen
                        if tags[screen][i] then
                            awful.tag.viewonly(tags[screen][i])
                        end
                  end),
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      if tags[screen][i] then
                          awful.tag.viewtoggle(tags[screen][i])
                      end
                  end),
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.movetotag(tags[client.focus.screen][i])
                      end
                  end),
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.toggletag(tags[client.focus.screen][i])
                      end
                  end))
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- here is a function created by Matthew Hague
-- to keep dialog boxes on current mouse screen
-- (where the mouse pointer is located at)
function totag(i)
   return function(c)
             local screentags = screen[mouse.screen]:tags()
             c:tags({screentags[i]})
             c.screen = mouse.screen
             awful.tag.viewonly(screentags[i])
          end
end


-- {{{ Rules
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = true,
                     keys = clientkeys,
                     buttons = clientbuttons } },
    { rule = { class = "MPlayer" },
      properties = { floating = true } },
    { rule = { class = "pinentry" },
      properties = { floating = true } },
    { rule = { class = "gimp" },
      properties = { floating = true } },
    -- Set Firefox to always map on tags number 2 of screen 1.
    -- { rule = { class = "Firefox" },
    --   properties = { tag = tags[1][2] } },

----------------------------here are my rules

    { rule = { class = "Firefox.*" , instance = "Download"   },  properties = { floating = true, tag = tags[1][2]} },
    { rule = { class = "Firefox.*" , instance = "Extension"  },  properties = { floating = true, tag = tags[1][2]} },
    { rule = { class = "Firefox.*" , instance = "Browser"    },  properties = { floating = true, tag = tags[1][2]} },
    { rule = { class = "Firefox.*" , instance = "Toplevel"   },  properties = { floating = true, tag = tags[1][2]} },
    { rule = { class = "Firefox.*" , instance = "Navigator"  },  properties = { tag = tags[1][2] } },
    { rule = { class = "Firefox.*" , instance = "Toolkit"    },  properties = { floating = true, tag = tags[1][2] } },
    
    
   { rule = { class = "GNU IceCat" , instance = "Navigator"  },  properties = {tag = tags[1][2]}},
   { rule = { class = "GNU IceCat" , instance = "Download"   },  properties = { floating = true, tag = tags[1][2]} },
   { rule = { class = "GNU IceCat" , instance = "Extension"  },  properties = { floating = true, tag = tags[1][2]} },
   { rule = { class = "GNU IceCat" , instance = "Browser"    },  properties = { floating = true, tag = tags[1][2]} },
   { rule = { class = "GNU IceCat" , instance = "Toplevel"    },  properties = { floating = true, tag = tags[1][2]} },
   
   { rule = { class = "Iceweasel" , instance = "Download"   },  properties = { floating = true, tag = tags[1][2]} },
   { rule = { class = "Iceweasel" , instance = "Extension"  },  properties = { floating = true, tag = tags[1][2]} },
   { rule = { class = "Iceweasel" , instance = "Browser"    },  properties = { floating = true, tag = tags[1][2]} },
   { rule = { class = "Iceweasel" , instance = "Toplevel"   },  properties = { floating = true, tag = tags[1][2]} },
   { rule = { name  = ".*Messenger.*" }, properties = { floating = true }},
   { rule = { class = "Iceweasel" , instance = "Navigator"  },  properties = { tag = tags[1][2] } },


   -- { rule = { class = "Iceweasel" , instance = "Download"   },  properties = { floating = true, tag = tags[1][2]} },
   -- { rule = { class = "Iceweasel" , instance = "Extension"  },  properties = { floating = true, tag = tags[1][2]} },
   -- { rule = { class = "Iceweasel"                           },  properties = { floating = true, tag = tags[1][2]} },

   { rule = { class = "Midori" , instance = "midori"  },         properties = {tag = tags[1][2]}},
   -- { rule = { class = "Chromium-browser" , instance = "chromium-browser"  },         properties = {tag = tags[1][2]}},
   { rule = { class = "Chromium.*"                       },         properties = { tag = tags[1][2], callback = totag(2)}},
   { rule = { class = "Chromium.*" , name = ".*Abrir.*"  },         properties = { callback = totag(2)}},
   -- { rule = { class = "Chromium" , instance = "chromium"  },         properties = {tag = tags[mouse.screen][2]}},

   { rule = { class = "Chromium.*", instance = ".*google.*"  },         properties = { callback = totag(2)}},

   --    { match = { "Navigator", "browser", "chrome", "Firefox", "Iceweasel", "GNU IceCat", "epiphany"} , tag = "web" } ,

   { rule = { class = "Icedove.*" , instance = "Mail"  },        properties = { tag = tags[screen.count()][2]} },
   { rule = { class = "Icedove.*" , instance = "Msgcompose"  },  properties = { floating = true, tag = tags[screen.count()][2]} },
   { rule = { class = "Icedove.*" , instance = "Mail", name = ".*Preferencias.*"  },  properties = { floating = true, tag = tags[screen.count()][2]} },
   { rule = { class = "Icedove.*"   },  properties = { floating = true, tag = tags[screen.count()][2]} },

   { rule = { class = "Gnome-terminal" }, properties = { floating = true, size_hints_honor = false, opacity = 0.9  } },
   { rule = { class = "URxvt" },          properties = { tag = tags[1][1], size_hints_honor = false, opacity = 0.9  } },
   { rule = { class = "XTerm" },          properties = { tag = tags[1][1], size_hints_honor = false, opacity = 0.9  } },

   { rule = { class = "Emacs", instance = "emacs" }, properties = {tag = tags[mouse.screen][6] , opacity = 0.9, size_hints_honor = false}},
   { rule = { class = "Emacs", instance = "Ediff" }, properties = {tag = tags[mouse.screen][6] , floating = true }},
   --["emacs"]  = { position = 6, exclusive = true, layout = awful.layout.suit.max, },

   { rule = { class = "Eclipse", instance = "Eclipse" }, properties = {tag = tags[1][4]}, floating = true },

   { rule = { class = "Evince" },             properties = {tag = tags[1][7]}},
   { rule = { class = "OpenOffice.org 3.2" }, properties = {tag = tags[1][7]}},
   { rule = { class = ".*LibreOffice.*" }, properties = {tag = tags[1][7]}},
   { rule = { class = "libreoffice-%l" }, properties = {tag = tags[1][7]}},

   { rule = { class = "Nautilus" },  properties = {tag = tags[1][9]}},
   { rule = { class = "Pcmanfm" },  properties = {tag = tags[1][9]}},
   { rule = { class = "Thunar" },  properties = {tag = tags[1][9]}},
   { rule = { class = "Thunar" , name = ".*Progres.*"},  properties = {tag = tags[1][9]}, floating = true },

   { rule = { class = "Pidgin" },  properties = {tag = tags[1][3]}},
   { rule = { class = "emesene" }, properties = {tag = tags[1][3]}},
   

   -- youtube fullscreen fix
   { rule = { instance = "plugin-container" },
     properties = { floating = true } },
   { rule = { instance = "exe" },
     properties = { floating = true } }, -- for chromium
   
   { rule = { class = "Vlc" }, properties = {tag = tags[1][5]}},
   -- { rule = { class = "Rhythmbox", instance = "rhythmbox" }, properties = {tag = tags[1][5]}},
   { rule = {                      instance = "rhythmbox" }, properties = {tag = tags[1][5]}},

   { rule = { class = "Gimp" },      properties = { floating = true, tag = tags[1][5] } }, --callback = awful.titlebar.add },
   { rule = { class = "Arandr" },    properties = { floating = true } }, --callback = awful.titlebar.add },
   { rule = { class = "Nitrogen" },  properties = { floating = true } }, 
   { rule = { class = "Eog" },  properties = { floating = true } }, 

   -- Set Alpine to tag 6 of the last screen
   --{ rule = { name = "Alpine" },    properties = {tag = tags[screen.count()][6]}},

   -- Set Akregator to tag 8 of the last screen and add a titlebar trough callback
   --{ rule = { class = "Akregator" },properties = {tag = tags[screen.count()][8]}, callback = awful.titlebar.add},

   -- Set Xterm to multiple tags on screen 1
   --{ rule = { class = "XTerm" }, callback = function(c) c:tags({tags[1][5], tags[1][6]}) end},

   -- Set ROX-Filer to tag 2 of the currently selected and active screen
   --{ rule = { class = "ROX-Filer" }, callback = function(c) awful.client.movetotag(tags[mouse.screen][2], c) end},

   -- Set Geeqie to the currently focused tag, as floating
   --{ rule = { instance = "geeqie" }, properties = {floating = true}},

   -- Set Xterm as floating with a fixed position
   --{ rule = { class = "XTerm" }, properties = {floating = true}, callback = function(c) c:geometry({x=0, y=0}) end},

   { rule = { class = "Pgadmin3", instance = "pgadmin3" },  properties = {tag = tags[screen.count()][4]}},
   { rule = { class = "Pgadmin3", instance = "pgadmin3", name = ".*Query.*" },  properties = { floating = true, tag = tags[screen.count()][4]}},
   { rule = { class = "Mysql-workbench-bin" },  properties = {tag = tags[screen.count()][4]}},

   { rule = { role = "pop-up"}, properties = { floating = true }},
   
   -- { rule = { class = "java-lang-Thread", instance = "sun-awt-X11-XDialogPeer" }, properties = { tag = tags[2][4] , opacity = 0.8 }},
   -- { rule = { class = "java-lang-Thread", instance = "sun-awt-X11-XFramePeer" }, properties = { tag = tags[2][4], opacity = 0.9 }}
   { rule = { name = ".*NetBeans.*" }, properties = { tag = tags[screen.count()][4], }},
   --"sun-awt-X11-XFramePeer",

   { rule = { name = ".*[Pp]y[Cc]harm.*" }, properties = { tag = tags[screen.count()][4], }},
   { rule = { class = ".*Thg.*" }, properties = { tag = tags[screen.count()][4], }},

   { rule = { name = ".*soapUI.*" }, properties = { tag = tags[screen.count()][4], }}



----------------------------here end my rules


}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.add_signal("manage", function (c, startup)
    -- Add a titlebar
    -- awful.titlebar.add(c, { modkey = modkey })

    -- Enable sloppy focus
    c:add_signal("mouse::enter", function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
            client.focus = c
        end
    end)

    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end
end)

client.add_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.add_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}
