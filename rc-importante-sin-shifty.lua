-- Standard awesome library
require("awful")
require("awful.autofocus")
require("awful.rules")
-- Theme handling library
require("beautiful")
-- Notification library
require("naughty")

-- Load Debian menu entries
require("debian.menu")

config = awful.util.getdir("config")

-- {{{ Variable definitions
-- Themes define colours, icons, and wallpapers
--beautiful.init("/usr/share/awesome/themes/sky/theme.lua")
beautiful.init(config .. "/themes/tom/theme.lua")

-- This is used later as the default terminal and editor to run.
terminal = "x-terminal-emulator"
editor = os.getenv("EDITOR") or "nano"
editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

--------------------------------------agregue yo
-- dynamic tagging
require("eminent")
--require("shifty")

--para manejar el mouse cn el teclado
--require("rodentbane") 

--expose?
--require("revelation")

-- para algunos widgets
require("wicked")
require("vicious")
require("obvious")

require("calendar2")

spacer = widget({ type = "textbox"  })
spacer.text = " | "

space = widget({ type = "textbox"  })
space.text = " "

--- pruebas wicked widgets
cpugraphwidget = widget({
    type = 'graph',
    name = 'cpugraphwidget',
    align = 'right'
})

cpugraphwidget.height = 0.85
cpugraphwidget.width = 45
cpugraphwidget.bg = '#333333'
cpugraphwidget.border_color = '#0a0a0a'
cpugraphwidget.grow = 'left'

cpugraphwidget:plot_properties_set('cpu', {
    fg = '#AEC6D8',
    fg_center = '#285577',
    fg_end = '#285577',
    vertical_gradient = false
})

wicked.register(cpugraphwidget, wicked.widgets.cpu, '$1', 1, 'cpu')


--batteries = 2

-- Function to extract charge percentage
--function read_battery_life(number)
--   return function(format)
--             local fh = io.popen('acpi')
--             local output = fh:read("*a")
--             fh:close()

 --            count = 0
 --            for s in string.gmatch(output, "(%d+)%%") do
 --               if number == count then
 --                  return {s}
 --               end
 --               count = count + 1
 --            end
 --         end
--end

-- Display one vertical progressbar per battery
---for battery=0, batteries-1 do
--battery = 0
--batterygraphwidget = widget({ type = 'progressbar',
--                              name = 'batterygraphwidget',
--                              align = 'right' })
--batterygraphwidget.height = 0.8
--batterygraphwidget.width = 5
--batterygraphwidget.bg = '#333333'
--batterygraphwidget.border_color = '#0a0a0a'
--batterygraphwidget.vertical = true
--batterygraphwidget:bar_properties_set('battery',
--                                      { fg = '#AEC6D8',
--                                        fg_center = '#285577',
--                                        fg_end = '#285577',
--                                        fg_off = '#222222',
--                                        vertical_gradient = true,
--                                        horizontal_gradient = false,
--                                        --ticks_count = 0,
--                                        --ticks_gap = 0 
--                                     })

--wicked.register(batterygraphwidget, read_battery_life(battery), '$1', 1, 'battery')
---end

--- fin pruebas wicked widgets

--- gmailwidget
--gmailwidget = widget({ type = "textbox"})
--vicious.register(gmailwidget, vicious.widgets.gmail, '${count}',30)
mygmail = widget({ type = "textbox" })
mygmail_t = awful.tooltip({ objects = { mygmail }, })
vicious.register(mygmail, vicious.widgets.gmail,
                 function (widget, args)
                    mygmail_t:set_text(args["{subject}"])
                    return 'gmail: <span color="white" weight="bold">'..args["{count}"]..'</span> | '
                 end, 20)


--- fin gmailwidget

--weather widget
weatherwidget = widget({ type = 'textbox' })
vicious.register(weatherwidget, vicious.widgets.weather, '${tempc}°C', 360, 'SCEL')
weatherwidget:add_signal('mouse::enter', function ()
                                            local statf = io.popen('weather -i SCEL -m')
                                            local stat = statf:read("*all")
                                            statf :close()
                                            weatherinfo = {
                                               naughty.notify({ 
                                                                 title      = "Weather"
                                                                 , text       = stat
                                                                 , timeout    = 0
                                                                 , position   = "top_right"
                                                             })
                                            }
                                         end)
weatherwidget:add_signal('mouse::leave', function () naughty.destroy(weatherinfo[1]) end)

--fin weather widget


--thermal widget
thermalwidget = widget({ type = 'textbox' }) 
vicious.register(thermalwidget, vicious.widgets.thermal, '$1°C', 5, 'thermal_zone0')
--fin thermal widget

---volume widget
--volumewidget = widget({ type = 'textbox' })
--vicious.register(volumewidget, vicious.widgets.volume, '$2$1%', nil, 'Master')
--volumewidget:buttons(awful.util.table.join(
--  awful.button({            }, 1, function () awful.util.spawn("amixer sset Master toggle") end),
--  awful.button({            }, 3, function () awful.util.spawn( terminal .. " -e alsamixer") end),
--  awful.button({            }, 4, function () awful.util.spawn("amixer sset Master 3%+") end),
--  awful.button({            }, 5, function () awful.util.spawn("amixer sset Master 3%-") end),
--  awful.button({ "Control", }, 4, function () awful.util.spawn("amixer sset Master 10%+") end),
--  awful.button({ "Control", }, 5, function () awful.util.spawn("amixer sset Master 10%-") end)
--))

---fin volume widget

--baticon widget
--iconos para baticon widget
beautiful.widget_baticon_bat1 = config .. "/icons/battery/bat1.png"
beautiful.widget_baticon_bat2 = config .. "/icons/battery/bat2.png"
beautiful.widget_baticon_bat3 = config .. "/icons/battery/bat3.png"
beautiful.widget_baticon_bat4 = config .. "/icons/battery/bat4.png"
beautiful.widget_baticon_bat5 = config .. "/icons/battery/bat5.png"
beautiful.widget_baticon_bat6 = config .. "/icons/battery/bat6.png"
beautiful.widget_baticon_bat7 = config .. "/icons/battery/bat7.png"
beautiful.widget_baticon_bat8 = config .. "/icons/battery/bat8.png"
beautiful.widget_baticon_bat9 = config .. "/icons/battery/bat9.png"
beautiful.widget_baticon_bat10 = config .. "/icons/battery/bat10.png"
beautiful.widget_baticon_bat11 = config .. "/icons/battery/bat11.png"

beautiful.widget_baticon_char1 = config .. "/icons/battery/char1.png"
beautiful.widget_baticon_char2 = config .. "/icons/battery/char2.png"
beautiful.widget_baticon_char3 = config .. "/icons/battery/char3.png"
beautiful.widget_baticon_char4 = config .. "/icons/battery/char4.png"
beautiful.widget_baticon_char5 = config .. "/icons/battery/char5.png"
beautiful.widget_baticon_char6 = config .. "/icons/battery/char6.png"
beautiful.widget_baticon_char7 = config .. "/icons/battery/char7.png"
beautiful.widget_baticon_char8 = config .. "/icons/battery/char8.png"
beautiful.widget_baticon_char9 = config .. "/icons/battery/char9.png"
beautiful.widget_baticon_char10 = config .. "/icons/battery/char10.png"

beautiful.widget_baticon_ac = config .. "/icons/battery/ac.png"


--iconos para baticon widget \o/
baticon = widget({ type = "imagebox" })
baticon_t = awful.tooltip({ objects = { baticon }, })
--baticon.image = image(beautiful.baticon_bat1)
vicious.register(baticon, vicious.widgets.bat, function (widget, args)
                                                  baticon_t:set_text(args[1] .. args[2] .. args[3])
                                                  local battery_presentf = io.popen('cat /sys/class/power_supply/BAT0/present ')
                                                  local battery_present = battery_presentf:read("*all")
                                                  battery_presentf :close()

                                                  local rv = { }
                                                  local fd = io.popen( "acpi -b")
                                                  if not fd then return end

                                                  local line = fd:read("*l")
                                                  while line do
                                                     local data = line:match("Battery #?[0-9] *: ([^\n]*)")

                                                     rv.state = data:match("([%a]*),.*"):lower()
                                                     rv.charge = tonumber(data:match(".*, ([%d]?[%d]?[%d]%.?[%d]?[%d]?)%%"))
                                                     rv.time = data:match(".*, ([%d]?[%d]?:?[%d][%d]:[%d][%d])")

                                                     if not rv.state:match("unknown") then break end
                                                     line = fd:read("*l")
                                                  end

                                                  fd:close()


                                                  local battery_status = ""
                                                  battery_status = rv.state
                                                  if not battery_status then 
                                                     baticon.image = image(beautiful.widget_baticon_ac)
                                                  elseif battery_status == "discharging" then

                                                     if rv.charge > 95 and rv.charge <= 100 then
                                                        baticon.image = image(beautiful.widget_baticon_bat1)
                                                     elseif rv.charge > 90 and rv.charge < 95 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_bat2)
                                                     elseif rv.charge >= 80 and rv.charge < 90 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_bat3)
                                                     elseif rv.charge >= 70 and rv.charge < 80 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_bat3)
                                                     elseif rv.charge >= 60 and rv.charge < 70 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_bat4)
                                                     elseif rv.charge >= 50 and rv.charge < 60 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_bat5)
                                                     elseif rv.charge >= 40 and rv.charge < 50 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_bat6)
                                                     elseif rv.charge >= 30 and rv.charge < 40 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_bat7)
                                                     elseif rv.charge >= 20 and rv.charge < 30 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_bat8)
                                                     elseif rv.charge >= 10 and rv.charge < 20 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_bat9)
                                                     elseif rv.charge > 5 and rv.charge < 10 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_bat10)

                                                     elseif rv.charge <= 5 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_bat11)
                                                     end

                                                  elseif battery_status == "charging" then
                                                     if rv.charge > 95 and rv.charge <= 100 then
                                                        baticon.image = image(beautiful.widget_baticon_char1)
                                                     elseif rv.charge > 90 and rv.charge < 95 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_char2)
                                                     elseif rv.charge >= 80 and rv.charge < 90 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_char3)
                                                     elseif rv.charge >= 70 and rv.charge < 80 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_char3)
                                                     elseif rv.charge >= 60 and rv.charge < 70 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_char4)
                                                     elseif rv.charge >= 50 and rv.charge < 60 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_char5)
                                                     elseif rv.charge >= 40 and rv.charge < 50 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_char6)
                                                     elseif rv.charge >= 30 and rv.charge < 40 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_char7)
                                                     elseif rv.charge >= 20 and rv.charge < 30 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_char8)
                                                     elseif rv.charge >= 10 and rv.charge < 20 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_char9)
                                                     elseif rv.charge > 5 and rv.charge < 10 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_char10)

                                                     elseif rv.charge <= 5 then
                                                     
                                                        baticon.image = image(beautiful.widget_baticon_char10)
                                                     end
                                                  elseif battery_status == "full" then
                                                       baticon.image = image(beautiful.widget_baticon_ac)

                                                  end


                                                    if battery_present == '0' then
                                                       baticon.image = image(beautiful.widget_baticon_ac)
                                                    end
                                                    
                                                    

                                                 end,
                 1, "BAT0")


--batwidget = widget({ type = "textbox" })
--vicious.register(batwidget, vicious.widgets.bat, "$1 - $2 - $3", 61, "BAT0")



--fin baticon widget

--cpugraphwidget
--cpugraphwidget = awful.widget.graph()
--cpugraphwidget:set_width(50)
--cpugraphwidget:set_background_color("#494B4F")
--cpugraphwidget:set_color("#FF5656")
--cpugraphwidget:set_gradient_colors({ "#FF5656", "#88A175", "#AECF96" })
--vicious.register(cpugraphwidget, vicious.widgets.cpu, "$1", 3)
--fin cpugraph

--cpuwidget
cpuwidget = widget({ type = 'textbox' })
vicious.register(cpuwidget, vicious.widgets.cpu, "$1%", 3,nil,{3,3})
cpuwidget:buttons(awful.util.table.join(
  awful.button({            }, 1, function () awful.util.spawn( terminal .. " -e htop") end)
))

cpuicon = widget({ type = 'imagebox' })
cpuicon.image = image(config .. "/icons/cpu.png")
--fin cpuwidget

--netwidget
-- Initialize widget
netwidget = widget({ type = "textbox" })
-- Register widget
vicious.register(netwidget, vicious.widgets.net, '<span color="#CC9393">${wlan0 down_kb}</span> <span color="#7F9F7F">${wlan0 up_kb}</span>', 3)

--iconos para netwidget
--beautiful.widget_netdown = config .. "/go-down12.png"
--beautiful.widget_netup   = config .. "/go-up12.png"
beautiful.widget_netdown = config .. "/icons/down.png"
beautiful.widget_netup   = config .. "/icons/up.png"

--iconos para netwidget \o/
dnicon = widget({ type = "imagebox" })
upicon = widget({ type = "imagebox" })
dnicon.image = image(beautiful.widget_netdown)
upicon.image = image(beautiful.widget_netup)

--caracteres para netwidget
--dncar = widget({ type = "textbox"})
--upcar = widget({ type = "textbox"})
--dncar.text = '<span color="#CC9393"> ▼ </span>'
--upcar.text = '<span color="#7F9F7F"> ▲</span>'

--fin netwidget

--wifiwidget
wifiwidget = widget({ type = 'textbox' })
vicious.register(wifiwidget, vicious.widgets.wifi, '${ssid}' , 30, 'wlan0')
wifiwidget:buttons(awful.util.table.join(
  awful.button({ }, 1, function () awful.util.spawn( terminal .. " -e wicd-curses") end)
))

--fin wifiwidget

--mpd widget

mpdwidget = widget({ type = 'textbox' })
vicious.register(mpdwidget, vicious.widgets.mpd,
    function (widget, args)
       local status = ""
       local song =  args["{Artist}"] .. ' - ' .. args["{Album}"] .. ' - ' .. args["{Title}"] 
       if     args["{state}"] == "Stop"  then 
          status = "Stopped"
       elseif args["{state}"] == "Play" then --and title ~= args["{Title}"]  then 
          status = "Playing: " .. song
       elseif args["{state}"] == "Pause" then --and title ~= args["{Title}"] then 
          status = "Paused: "  .. song 
       end
       --return '<span color="white"> MPD:</span> '.. status 
       --args["{volume}"] .. "%"

       return status
    end,1)

--mpdwidget:add_signal('mouse::enter', function ()
--                                        local statf = io.popen('mpc -f "\n[[Artist: %artist% \nAlbum:  %album% \nTrack: %track%]\nTitle:  %title%]|[%file%]" ')
--                                        local stat = statf:read("*all")
--                                        statf :close()
--                                        naughty.notify({ title      = "Now Playing"
--                                                         , text       = stat
--                                                        , timeout    = 5
--                                                         , position   = "bottom_left"
--                                                      })
--                                     end)

mpdwidget:buttons({
                     button({ }, 1, function () awful.util.spawn("mpc toggle") end),
                     button({ }, 2, function () awful.util.spawn("mpc stop") end),
                     button({ }, 3, function () awful.util.spawn(terminal .. " -e ncmpcpp") end),
                     button({ }, 4, function () awful.util.spawn("mpc prev") end),
                     button({ }, 5, function () awful.util.spawn("mpc next") end),
                     button({"Control" }, 4, function () awful.util.spawn("mpc volume +3") end),
                     button({"Control" }, 5, function () awful.util.spawn("mpc volume -3") end)
    })

mpdicon = widget({ type = 'imagebox' })
mpdicon.image = image(config .. "/icons/mpd.png")
--fin mpdwidget

--tasklauncher widget
mytaskslauncher = widget({ type = "imagebox" })
mytaskslauncher.image = image(beautiful.awesome_icon)
mytaskslauncher:buttons(awful.util.table.join(
  awful.button({ }, 1, function ()
                          if instance then
                             instance:hide()
                             instance = nil
                          else
                             instance = awful.menu.clients({ width=250 })
                          end
                       end)
))
--fin tasklauncher widget

---obvious clock
--obvious.clock.set_editor("xterm -e zile")
--obvious.clock.set_shortformat(" %T %a %b %d")
--obvious.clock.set_longformat(function () return " %T %a %b %d %Y" end)
--obvious.clock.set_shorttimer(1)
---fin clock

-- at startup :)
os.execute("setxkbmap us -variant altgr-intl &")
os.execute("mpd &")

--localization!
os.setlocale("es_CL.UTF-8")

-----------------------------------hasta aqui agregue yo


-- Table of layouts to cover with awful.layout.inc, order matters.
layouts =
{
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.spiral,
    awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
    awful.layout.suit.magnifier
}
-- }}}

-- {{{ Tags
-- Define a tag table which hold all screen tags.
tags = {}
for s = 1, screen.count() do
    -- Each screen has its own tag table.
    tags[s] = awful.tag({ 1, 2, 3, 4, 5, 6, 7, 8, 9 }, s, layouts[1])
end
-- }}}

-- {{{ Menu
-- Create a laucher widget and a main menu
myawesomemenu = {
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awful.util.getdir("config") .. "/rc.lua" },
   { "restart", awesome.restart },
   { "quit", awesome.quit }
}

--mio
mysystemmenu = {
   { "suspend" , "sudo pm-suspend" },
   { "hibernate" , "sudo pm-hibernate" },
   { "reboot" , "sudo shutdown -r now" },
   { "shutdown" , "sudo shutdown -h now" }
}
--fin mio
freqapps = {
   { "emacs", "emacs" },
   { "claws-mail" , "claws-mail" },
   { "pidgin" , "pidgin" },
   { "icecat" , "icecat" },
   { "ncmpcpp" , terminal .. " -e ncmpcpp" }
}

--apps frecuentes

--fin apps frecuentes

mymainmenu = awful.menu({ items = { { "awesome", myawesomemenu, beautiful.awesome_icon },
                                    { "frecuentes", freqapps },
                                    { "Debian", debian.menu.Debian_menu.Debian },
                                    { "System", mysystemmenu },
                                    { "open terminal", terminal }
                                  }
                        })

mylauncher = awful.widget.launcher({ image = image(beautiful.awesome_icon),
                                     menu = mymainmenu })

-- }}}

-- {{{ Wibox
-- Create a textclock widget
mytextclock = awful.widget.textclock({ align = "right" })

calendar2.addCalendarToWidget(mytextclock, "<span color='green'>%s</span>")

-- Create a systray
mysystray = widget({ type = "systray" })

-- Create a wibox for each screen and add it
mywibox = {}
myotherwibox = {}
mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
                    awful.button({ }, 1, awful.tag.viewonly),
                    awful.button({ modkey }, 1, awful.client.movetotag),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, awful.client.toggletag),
                    awful.button({ }, 4, awful.tag.viewnext),
                    awful.button({ }, 5, awful.tag.viewprev)
                    )
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if not c:isvisible() then
                                                  awful.tag.viewonly(c:tags()[1])
                                              end
                                              client.focus = c
                                              c:raise()
                                          end),
                     awful.button({ }, 3, function ()
                                              if instance then
                                                  instance:hide()
                                                  instance = nil
                                              else
                                                  instance = awful.menu.clients({ width=250 })
                                              end
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                              if client.focus then client.focus:raise() end
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                              if client.focus then client.focus:raise() end
                                          end))

for s = 1, screen.count() do
    -- Create a promptbox for each screen
    mypromptbox[s] = awful.widget.prompt({ layout = awful.widget.layout.horizontal.leftright })
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    mylayoutbox[s] = awful.widget.layoutbox(s)
    mylayoutbox[s]:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
                           awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)))
    -- Create a taglist widget
    mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.label.all, mytaglist.buttons)

    -- Create a tasklist widget
    mytasklist[s] = awful.widget.tasklist(function(c)
                                              return awful.widget.tasklist.label.currenttags(c, s)
                                          end, mytasklist.buttons)

    -- Create the wibox
    mywibox[s] = awful.wibox({ position = "top", screen = s , height = 12 })
    -- Add widgets to the wibox - order matters
    mywibox[s].widgets = {
        {
           mylayoutbox[s],
--          mylauncher,
           mytaglist[s],
           mypromptbox[s],
           layout = awful.widget.layout.horizontal.leftright
        },
--        mylayoutbox[s],
        mytaskslauncher,
        mytextclock,
--        obvious.clock(),
        weatherwidget,
        space,
        s == 1 and mysystray or nil,
        space,
--        upcar, netwidget, dncar,
--        upicon, netwidget, dnicon,
--        wifiwidget,
        mytasklist[s],
        layout = awful.widget.layout.horizontal.rightleft
    }
--    mywibox[s].height = 12 --in pixels  :D
    
    --other wibox :)
    myotherwibox[s] = awful.wibox({ position = "bottom", screen = s, height = 12 })
    -- Add widgets to the wibox - order matters
    myotherwibox[s].widgets = {
       {
          mpdicon,
          space,
          mpdwidget,
          space, obvious.volume_alsa(),
          layout = awful.widget.layout.horizontal.leftright
       },
       obvious.loadavg(),
       space, cpugraphwidget,
       space, cpuwidget, cpuicon,
--     space, obvious.temp_info(),
       space, thermalwidget,
       space,
--     upcar, netwidget, dncar,
       upicon, netwidget, dnicon,
       space, wifiwidget,
--     spacer, batwidget,
       space,
       obvious.battery(),
       baticon,
       space, batterygraphwidget,
       mygmail,
       layout = awful.widget.layout.horizontal.rightleft
    }
end
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(
--my bindings
    awful.key({                   }, "Print", function() awful.util.spawn("gnome-screenshot") end),
    awful.key({ "Mod1",           }, "Print", function() awful.util.spawn("gnome-screenshot -w") end),
    awful.key({ modkey,           }, "F8", function() awful.util.spawn("arandr") end),
--  awful.key({ modkey,           }, "e",  revelation.revelation),
--end my bindings
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev       ),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext       ),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "w", function () mymainmenu:show({keygrabber=true})        end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.util.spawn(terminal) end),
    awful.key({ modkey, "Control" }, "r", awesome.restart),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)    end),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1)      end),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1)      end),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1)         end),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1)         end),
    awful.key({ modkey,           }, "space", function () awful.layout.inc(layouts,  1) end),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(layouts, -1) end),

    -- Prompt
    awful.key({ modkey },            "r",     function () mypromptbox[mouse.screen]:run() end),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run({ prompt = "Run Lua code: " },
                  mypromptbox[mouse.screen].widget,
                  awful.util.eval, nil,
                  awful.util.getdir("cache") .. "/history_eval")
              end)
)

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, "o",      awful.client.movetoscreen                        ),
    awful.key({ modkey, "Shift"   }, "r",      function (c) c:redraw()                       end),
    awful.key({ modkey,           }, "n",      function (c) c.minimized = not c.minimized    end),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end)
)

-- Compute the maximum number of digit we need, limited to 9
keynumber = 0
for s = 1, screen.count() do
   keynumber = math.min(9, math.max(#tags[s], keynumber));
end

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, keynumber do
    globalkeys = awful.util.table.join(globalkeys,
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = mouse.screen
                        if tags[screen][i] then
                            awful.tag.viewonly(tags[screen][i])
                        end
                  end),
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      if tags[screen][i] then
                          awful.tag.viewtoggle(tags[screen][i])
                      end
                  end),
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.movetotag(tags[client.focus.screen][i])
                      end
                  end),
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.toggletag(tags[client.focus.screen][i])
                      end
                  end))
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = true, --true
                     --lower = true, --agregué yo, para que la ventana quede atrás!!!!!
                     keys = clientkeys,
                     buttons = clientbuttons } },
    { rule = { class = "gimp" },
      properties = { floating = true } },
    --{ rule = { class = "Pidgin" }, properties = { lower = true } }
    -- Set app to always map on tags number 2 of screen 1.
    --{ rule = { class = "Emacs", instance = "emacs" },
    -- properties = { tag = tags[1][2] } ,
    --  callback = function(c) c.maximized_horizontal = true c.maximized_vertical = true  end },
    
    --{ rule = { class = "Claws-mail", instance = "claws-mail" },
    --properties = { opacity = 0.6 } },
    --  callback = function(c) c:geometry({x=20, y=20}) c.maximized_vertical = true  end },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.

client.add_signal("manage", function (c, startup)
    -- Add a titlebar
--    if client.floating(c) == true then
--       awful.titlebar.add(c, { modkey = modkey })
--    end

    -- Enable sloppy focus
    c:add_signal("mouse::enter", function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
            client.focus = c
         end
    end)

   -- c:add_signal("new", function(c)
   --                        c:lower() -- = c
   -- end)


    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end
end)

client.add_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.add_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}
